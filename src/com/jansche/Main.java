package com.jansche;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String[] words = {"TOMATE", "OPERATOR", "KÜSTE", "DESTRUKTIV", "ENTROPIE", "EINBRUCH", "VERBOTEN", "BLUTKREISLAUF", "FEUERWERKSKÖRPER", "SCHLAF", "KRIECHPFLANZE"};
        Scanner scanner = new Scanner(System.in);
        char[] wordarray;
        char[] word;
        char letter;
        System.out.println("Sind Sie bereit das Wort zu erraten?");
        int random = (int)(Math.random() * words.length);
        wordarray = new char[words[random].length()];
        word = new char[words[random].length()];
        for (int i = 0; i < words[random].length(); i++) {
            wordarray[i] = '_';
            word[i] = words[random].charAt(i);
        }
        print(wordarray, word, words, random);
        System.out.println("Raten Sie einen Buchstaben:");
        letter = scanner.nextLine().charAt(0);
        //System.out.println(letter);
        List<Integer> letterInstances = new ArrayList<Integer>(10);
        for (int i = 0; i < words[random].length(); i++) {
            if (words[random].indexOf(letter, i) == 0) {
                letterInstances.add(i);
            }
        }
        System.out.println(words[random].indexOf(letter));
        System.out.println(Arrays.toString(letterInstances.toArray()));
        for (Integer number : letterInstances) {
            System.out.println("Number = " + number);
        }
        //wordbuild = "";
        //for (int i = 0; i < words[random].indexOf())
    }

    public static void print(char[] wordarray, char[] word, String[] words, int random) {
        for (int i = 0; i < words[random].length(); i++) {
            System.out.print(wordarray[i] + " ");
        }
        System.out.println();
        System.out.println(word);
    }
}
